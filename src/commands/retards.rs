use include_dir::{include_dir, Dir};
use serenity::{
    framework::standard::{macros::command, CommandResult},
    model::prelude::Message,
    prelude::Context,
};
use tracing::info;

const PROJECT_DIR: Dir<'_> = include_dir!("retards_faces");

#[command]
#[aliases("retard")]
pub async fn retard(ctx: &Context, msg: &Message) -> CommandResult {
    let random_value: usize = rand::random::<usize>() % PROJECT_DIR.files().count();

    let file = PROJECT_DIR
        .files()
        .nth(random_value)
        .expect("randon value outside files");

    msg.channel_id
        .send_message(ctx, |m| {
            m.content("gneh")
                .add_file((
                    file.contents(),
                    file.path()
                        .file_name()
                        .expect("Failed to retrieve file name")
                        .to_str()
                        .expect("Failed to convert OsStr to &str"),
                ))
                .reference_message(msg)
        })
        .await?;
    info!("Sent retard image to {}", msg.author.name);

    Ok(())
}
