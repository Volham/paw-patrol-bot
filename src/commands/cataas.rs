use serenity::{
    framework::standard::{macros::command, Args, CommandError, CommandResult},
    model::prelude::Message,
    prelude::Context,
};
use tracing::info;

async fn send_cat_command(text: Option<String>) -> Result<Vec<u8>, CommandError> {
    if text.is_none() {
        Ok(reqwest::get("https://cataas.com/cat")
            .await?
            .bytes()
            .await?
            .to_vec())
    } else {
        Ok(reqwest::get(format!(
            "https://cataas.com/cat/says/{}",
            urlencoding::encode(text.as_ref().unwrap())
        ))
        .await?
        .bytes()
        .await?
        .to_vec())
    }
}

#[command]
pub async fn cat(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {
    let args_string = if !args.is_empty() {
        let mut arg_string = String::new();

        while let Ok(s) = args.single::<String>() {
            arg_string.push_str(&s);

            if args.len() > 1 {
                arg_string.push(' ');
            }
        }

        Some(arg_string)
    } else {
        None
    };

    let cat_image = send_cat_command(args_string).await?;
    info!(
        "Reply cat image: '{}' length: {}",
        msg.author.name,
        cat_image.len()
    );

    msg.channel_id
        .send_message(ctx, |m| {
            m.content("Meow 🐈!")
                .add_file((cat_image.as_slice(), "cat.png"))
                .reference_message(msg)
        })
        .await?;

    Ok(())
}
