use serenity::framework::standard::{macros::command, CommandResult};
use serenity::{model::prelude::Message, prelude::Context};

#[command]
async fn help(ctx: &Context, msg: &Message) -> CommandResult {
    msg.channel_id
        .send_message(ctx, |m| {
            m.embed(|e| {
                e.title("Paw patrol bot help")
                    .description("Each commands must be prefixed with `$`")
                    .field("chuck", "A chuck norris quote", false)
                    .field(
                        "cat [text]",
                        "Sends a cat with an optional custom text",
                        false,
                    )
                    .field(
                        "joke [random] | [nsfw] | [category]",
                        "Sends a joke. The available categories are: global, dev, dark, limit, beauf, blondes. ⚠ Some NSFW jokes may shock some people use it at your OWN risks! ⚠",
                        false,
                    )
                    .field(
                        "meme [subreddit <subreddit name>]",
                        "Get a cool meme from reddit. You can specify a specific subreddit to query, default is memes.",
                        false,
                    )
                    .field("retard", "sends a retard image for retarded people", false)
                    .color((209, 132, 17))
                    .footer(|f| f.text("If you have any feature idea please share!"))
            })
            .reference_message(msg)
        })
        .await?;

    Ok(())
}
