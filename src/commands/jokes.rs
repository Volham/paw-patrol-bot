use lazy_static::lazy_static;
use rand::Rng;
use serde::Deserialize;
use serenity::{
    framework::standard::{macros::command, Args, CommandResult},
    model::prelude::Message,
    prelude::Context,
};
use tracing::info;

#[derive(Deserialize, Debug, PartialEq)]
enum JokeType {
    #[serde(rename = "global")]
    Global,
    #[serde(rename = "dev")]
    Dev,
    #[serde(rename = "dark")]
    Dark,
    #[serde(rename = "limit")]
    Limit,
    #[serde(rename = "beauf")]
    Beauf,
    #[serde(rename = "blondes")]
    Blondes,
}

impl TryFrom<&str> for JokeType {
    type Error = &'static str;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        match value {
            "global" => Ok(JokeType::Global),
            "dev" => Ok(JokeType::Dev),
            "dark" => Ok(JokeType::Dark),
            "limit" => Ok(JokeType::Limit),
            "beauf" => Ok(JokeType::Beauf),
            "blondes" => Ok(JokeType::Blondes),
            _ => Err("No such category!"),
        }
    }
}

#[derive(Deserialize, Debug)]
#[allow(dead_code)]
struct Joke {
    id: usize,
    r#type: JokeType,
    joke: String,
    answer: Option<String>,
}

lazy_static! {
    static ref JOKES: Vec<Joke> =
        serde_json::from_str(include_str!("jokes.json")).expect("Failed to parse jokes json!");
}

fn get_random_joke_from_type(joke_type: &[JokeType]) -> &'static Joke {
    let mut rng: rand::rngs::ThreadRng = rand::thread_rng();
    let random_value: usize = rng.gen();

    let jokes: Vec<&Joke> = JOKES
        .iter()
        .filter(|j| joke_type.iter().any(|t| t == &j.r#type))
        .collect();
    jokes
        .get(random_value % jokes.len())
        .expect("Fail to retrieve a joke. Is the category empty?")
}

async fn send_joke(ctx: &Context, msg: &Message, joke: &Joke) -> CommandResult {
    info!("Send joke to {}: joke={:?}", msg.author.name, joke);
    let reply_message = if let Some(ans) = &joke.answer {
        format!("{}\n... {}", &joke.joke, ans)
    } else {
        joke.joke.to_string()
    };

    msg.reply_ping(ctx, reply_message).await?;
    Ok(())
}

#[command]
#[aliases("random")]
pub async fn random_joke(ctx: &Context, msg: &Message) -> CommandResult {
    send_joke(
        ctx,
        msg,
        get_random_joke_from_type(&[JokeType::Global, JokeType::Dev]),
    )
    .await
}

#[command]
#[aliases("nsfw")]
pub async fn nsfw_joke(ctx: &Context, msg: &Message) -> CommandResult {
    send_joke(
        ctx,
        msg,
        get_random_joke_from_type(&[
            JokeType::Dark,
            JokeType::Limit,
            JokeType::Blondes,
            JokeType::Beauf,
        ]),
    )
    .await
}

#[command]
#[aliases("category")]
pub async fn category_joke(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {
    let mut joke_categories = Vec::new();
    while let Ok(s) = args.single::<String>() {
        match JokeType::try_from(s.as_str()) {
            Ok(j) => joke_categories.push(j),
            Err(error_msg) => {
                msg.reply_ping(ctx, error_msg).await?;
            }
        }
    }

    send_joke(ctx, msg, get_random_joke_from_type(&joke_categories)).await?;
    Ok(())
}
