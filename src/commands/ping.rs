use serenity::framework::standard::{macros::command, CommandResult};
use serenity::{model::prelude::Message, prelude::Context};
use tracing::info;

#[command]
#[owners_only]
async fn ping(ctx: &Context, msg: &Message) -> CommandResult {
    if let Err(err) = msg.reply(ctx, "Pong!").await {
        eprintln!("Failed to reply ping: {}", err);
        Err(err.into())
    } else {
        info!("Reply pong to {}", msg.author.name);
        Ok(())
    }
}

#[command]
#[owners_only]
async fn pong(ctx: &Context, msg: &Message) -> CommandResult {
    if let Err(err) = msg.reply(ctx, "Ping!").await {
        eprintln!("Failed to reply pong: {}", err);
        Err(err.into())
    } else {
        info!("Reply ping to {}", msg.author.name);
        Ok(())
    }
}
