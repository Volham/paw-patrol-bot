use serde::Deserialize;
use serenity::{
    framework::standard::{macros::command, Args, CommandError, CommandResult},
    model::prelude::{AttachmentType, Message},
    prelude::Context,
};
use tracing::{debug, error, info};

const MEME_API_URL: &str = "https://meme-api.com/gimme";

#[derive(Debug, Deserialize)]
#[allow(dead_code)]
struct MemeApiResponse {
    #[serde(rename(deserialize = "postLink"))]
    post_link: String,
    subreddit: String,
    title: String,
    url: String,
    nsfw: bool,
    spoiler: bool,
    author: String,
    ups: u64,
    preview: Vec<String>,
}

async fn get_meme_infos(subreddit: Option<&str>) -> Result<Option<MemeApiResponse>, CommandError> {
    let result = reqwest::get(if let Some(sub) = subreddit {
        format!("{MEME_API_URL}/{sub}")
    } else {
        MEME_API_URL.to_string()
    })
    .await?;

    if !result.status().is_success() {
        error!(
            "Call to reddit scrapper API failed with status: {:?}",
            result.status()
        );

        return Ok(None);
    }

    Ok(Some(result.json::<MemeApiResponse>().await?))
}

async fn download_meme_image(url: &str) -> Result<Option<Vec<u8>>, CommandError> {
    let result = reqwest::get(url).await?;

    if !result.status().is_success() {
        error!("Failed to download image {}", url);
        return Ok(None);
    }

    Ok(Some(result.bytes().await?.to_vec()))
}

async fn send_meme_embed_and_attachment(
    ctx: &Context,
    msg: &Message,
    meme_info: &MemeApiResponse,
    image_bytes: &[u8],
) -> CommandResult {
    msg.channel_id
        .send_message(ctx, |m| {
            m.embed(|e| {
                e.title(&meme_info.title)
                    .description("Scrapped from reddit")
                    .url(&meme_info.post_link)
                    .field(
                        "NSFW note",
                        if meme_info.nsfw {
                            "This is nsfw"
                        } else {
                            "This is sfw"
                        },
                        false,
                    )
                    .field("Upvotes count", meme_info.ups.to_string(), false)
                    .image(&meme_info.url)
                    .author(|a| a.name(&meme_info.author))
                    .color((255, 88, 26)) // reddit color is #ff581a
                    .footer(|f| f.text("Provided by meme-api"))
            })
            .add_file(AttachmentType::Bytes {
                data: image_bytes.into(),
                filename: meme_info.url[meme_info.url.rfind('/').unwrap()..].to_string(),
            })
            .reference_message(msg)
        })
        .await?;

    Ok(())
}

#[command]
#[aliases("subreddit")]
pub async fn subreddit_joke(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {
    if let Ok(ref s) = args.single::<String>() {
        match get_meme_infos(Some(s)).await? {
            Some(meme_info) => match download_meme_image(&meme_info.url).await? {
                Some(image_bytes) => {
                    info!(
                        "Send meme of subreddit '{}' to {} meme: {:?}",
                        s, msg.author.name, &meme_info
                    );
                    send_meme_embed_and_attachment(ctx, msg, &meme_info, &image_bytes).await?;
                }
                None => {
                    msg.reply_ping(ctx, "Failed to download meme image!")
                        .await?;
                }
            },
            None => {
                msg.reply_ping(ctx, "Failed to scrap reddit. Cringe :(")
                    .await?;
            }
        }
    } else {
        info!("{} bad request: missing subreddit name", msg.author.name);
        msg.reply_ping(ctx, "You need to specify a subreddit name!")
            .await?;
    }

    Ok(())
}

#[command]
#[aliases("meme")]
pub async fn meme(ctx: &Context, msg: &Message) -> CommandResult {
    match get_meme_infos(None).await? {
        Some(meme_info) => {
            info!("Send meme to {} meme: {:?}", msg.author.name, &meme_info);

            debug!(
                "Author: {} Image name: {}",
                meme_info.author,
                meme_info.url[meme_info.url.rfind('/').unwrap()..].to_string()
            );

            match download_meme_image(&meme_info.url).await? {
                Some(image_bytes) => {
                    send_meme_embed_and_attachment(ctx, msg, &meme_info, &image_bytes).await?;
                }
                None => {
                    msg.reply_ping(ctx, "Failed to download meme image!")
                        .await?;
                }
            }
        }
        None => {
            msg.reply_ping(ctx, "Failed to scrap reddit. Cringe :(")
                .await?;
        }
    }

    Ok(())
}
