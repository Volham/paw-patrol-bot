use std::env;

use lettre::{
    message::header::ContentType,
    transport::smtp::{authentication::Credentials, SUBMISSIONS_PORT},
    AsyncSmtpTransport, AsyncTransport, Tokio1Executor,
};
use serenity::{
    framework::standard::{macros::command, CommandResult},
    http::CacheHttp,
    model::prelude::{Channel, Message},
    prelude::Context,
};
use tracing::{debug, error, info};

// Stolen from https://kerkour.com/rust-send-email
async fn send_email_smtp(
    mailer: &AsyncSmtpTransport<Tokio1Executor>,
    from: &str,
    to: &str,
    subject: &str,
    body: String,
) -> Result<(), Box<dyn std::error::Error>> {
    let email = lettre::Message::builder()
        .from(from.parse()?)
        .to(to.parse()?)
        .subject(subject)
        .header(ContentType::TEXT_PLAIN)
        .body(body.to_string())?;

    mailer.send(email).await?;

    Ok(())
}

async fn send_mail(content: &str) -> CommandResult {
    let user_name = env::var("SMTP_USERNAME").expect("Define SMTP_USERNAME");
    let password = env::var("SMTP_PASSWORD").expect("Define SMTP_PASSWORD");
    let student_mails = env::var("STUDENT_MAILS").expect("Define student mails");

    let credentials = Credentials::new(user_name, password);
    let mailer: AsyncSmtpTransport<Tokio1Executor> =
        AsyncSmtpTransport::<Tokio1Executor>::relay("smtp.gmail.com")?
            .credentials(credentials)
            .port(SUBMISSIONS_PORT)
            .build();

    let from = "Délégués SSIE <ssie.toulouse@gmail.com>";
    let subject = "Message des délégués de SSIE";

    for mail in student_mails.split(';') {
        debug!("Send mail to {mail}");
        send_email_smtp(&mailer, from, mail, subject, content.to_string())
            .await
            .unwrap();
        debug!("Mail sent to {mail}");
    }

    Ok(())
}

#[command]
#[owners_only]
pub async fn news(ctx: &Context, msg: &Message) -> CommandResult {
    info!("Received news command from: {}", msg.author.name);

    match msg.message_reference.as_ref() {
        Some(msg_ref) => {
            info!("Message reference: {msg_ref:?}");
            let channel = msg.channel((&ctx.cache, ctx.http())).await?;
            match channel {
                Channel::Guild(guild_channel) => {
                    match guild_channel
                        .message(ctx, msg_ref.message_id.unwrap())
                        .await
                    {
                        Ok(msg) => match send_mail(&msg.content).await {
                            Ok(_) => {
                                info!("Mail sent");
                                msg.reply_ping(ctx, "Sucessfully sent mails to everyone")
                                    .await?;
                                Ok(())
                            }
                            Err(e) => {
                                error!("Failed to send mail: {e:?}");
                                msg.reply_ping(ctx, "Failed to send mails to everyone ask the bot's owner to check logs").await?;
                                Ok(())
                            }
                        },
                        Err(e) => {
                            error!("Failed to get channel message: {e:?}");
                            msg.reply_ping(ctx, "Failed to get referenced message from discord")
                                .await?;
                            Ok(())
                        }
                    }
                }
                _ => {
                    msg.reply_ping(ctx, "Failed to retrieve referenced message content")
                        .await?;
                    Ok(())
                }
            }
        }
        None => {
            msg.reply_ping(ctx, "Please reply to your original message to send it")
                .await?;
            Ok(())
        }
    }
}
