use serde::Deserialize;
use serenity::{
    framework::standard::{macros::command, CommandResult},
    model::prelude::Message,
    prelude::Context,
};
use tracing::log::info;

const RANDOM_JOKES_URI: &str = "https://api.chucknorris.io/jokes/random";

#[derive(Deserialize)]
#[allow(dead_code)] // Some fields are needed by serde to deserialize
struct ChuckNorrisApiResponse {
    pub icon_url: String,
    pub id: String,
    pub url: String,
    pub value: String,
}

#[command]
pub async fn chuck(ctx: &Context, msg: &Message) -> CommandResult {
    let response = reqwest::get(RANDOM_JOKES_URI)
        .await?
        .json::<ChuckNorrisApiResponse>()
        .await?;

    info!(
        "Reply chuck norris: '{}' joke: '{}'",
        msg.author.name, response.value
    );

    msg.reply_ping(ctx, response.value).await?;

    Ok(())
}
