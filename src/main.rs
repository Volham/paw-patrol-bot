use std::env;
use std::sync::Arc;
use std::{collections::HashSet, io};

mod commands;

use regex::Regex;
use serenity::{
    async_trait,
    client::bridge::gateway::ShardManager,
    framework::standard::{macros::group, StandardFramework},
    http::Http,
    model::prelude::{Message, Ready},
    prelude::*,
};
use tracing::{error, info, log::warn, Level};

use crate::commands::*;

#[group]
#[commands(ping, pong, chuck, cat, help, news, retard)]
struct General;

#[group]
#[prefix("meme")]
#[commands(meme, subreddit_joke)]
#[default_command(meme)]
struct Meme;

#[group]
#[prefix("joke")]
#[commands(random_joke, nsfw_joke, category_joke)]
#[default_command(random_joke)]
struct Joke;

pub struct ShardManagerContainer;

impl TypeMapKey for ShardManagerContainer {
    type Value = Arc<Mutex<ShardManager>>;
}

struct Handler {
    regex: Regex,
}

impl Default for Handler {
    fn default() -> Self {
        Self {
            // FIXME: This regex could be improved to match more cases
            regex: Regex::new(r".*\s*quoi\s*[?!.]*\s*$").expect("Failed to build regex!"),
        }
    }
}

#[async_trait]
impl EventHandler for Handler {
    async fn ready(&self, _: Context, ready: Ready) {
        info!("Connected as {}#{}", &ready.user.name, &ready.user.id);
    }

    /// Just here to reply feur automatically, useless otherwise
    async fn message(&self, ctx: Context, msg: Message) {
        if self.regex.is_match(&msg.content.to_lowercase()) {
            if let Err(err) = msg.reply_ping(ctx, "feur").await {
                warn!(
                    "Failed to send 'feur' reply to {} with the error {}",
                    msg.author.name, err
                );
            }
        }
    }
}

#[tokio::main]
async fn main() {
    // Initialize the logger to use environment variables.
    //
    // In this case, a good default is setting the environment variable
    // `RUST_LOG` to `debug`.
    tracing_subscriber::fmt::fmt()
        .with_max_level(Level::INFO)
        .with_target(false)
        .with_writer(io::stderr)
        .init();

    let token = env::var("DISCORD_TOKEN")
        .expect("Expected a token in the environment. Please set 'DISCORD_TOKEN' variable.");

    let http = Http::new(&token);

    // We will fetch your bot's owners and id
    let (owners, _bot_id) = match http.get_current_application_info().await {
        Ok(info) => {
            let mut owners = HashSet::new();
            owners.insert(info.owner.id);

            (owners, info.id)
        }
        Err(why) => panic!("Could not access application info: {:?}", why),
    };

    // Create the framework
    let framework = StandardFramework::new()
        .configure(|c| c.owners(owners).prefix("$"))
        .group(&GENERAL_GROUP)
        .group(&JOKE_GROUP)
        .group(&MEME_GROUP);

    let intents = GatewayIntents::GUILD_MESSAGES
        | GatewayIntents::DIRECT_MESSAGES
        | GatewayIntents::MESSAGE_CONTENT;
    let mut client = Client::builder(&token, intents)
        .framework(framework)
        .event_handler(Handler::default())
        .await
        .expect("Err creating client");

    {
        let mut data = client.data.write().await;
        data.insert::<ShardManagerContainer>(client.shard_manager.clone());
    }

    let shard_manager = client.shard_manager.clone();

    tokio::spawn(async move {
        tokio::signal::ctrl_c()
            .await
            .expect("Could not register ctrl+c handler");
        shard_manager.lock().await.shutdown_all().await;
    });

    if let Err(why) = client.start().await {
        error!("Client error: {:?}", why);
    }
}
